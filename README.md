# Casa de cambios

Se trata de un programa que por medio de la ventana de comandos puedes realizar cambios de diferentes divisas, en las que se encuentran las siguientes:

* Peso colombia (COP)
* Dólar estadounidense (USD)
* Libra esterlina (GBP)
* Euro (EUR)

---

## Aspecto visual del programa

* Menú de elección:
    ![texto](/Images/Prueba1.png)

* Resultado desplegado en el navegadro predeterminado:
    ![](/Images/Prueba2.png)

---

## ¿Cómo se usa?

1. ### Descarga:
    En la carpeta "programas" puedes descargar el ejecutable o bien lo puedes editar descargando el archivo ***"casa de cambios.bat"***
    ![](/Images/Descarga1.png)

    Si sale un mensaje sobre que el archivo no se puede descargar hasta que se confirme que se confía en este, presiona click en *conservar*.
    ![](/Images/Descarga2.png)

    Si sale otro menú, da click en ***"Conservar de todos modos"***:
    ![](/Images/Descarga3.png)

    Esto sucede porque estás descargando un programa que abre la ventana de comandos (CMD) y el dispositivo piensa que se puede tratar de un virus, pero eso **NO** es así 🙂

2. ### Ejecucuión:
    Si salta una ventana emergente al intentar ejecutar el archivo diciendo *"Windows protegió su PC"*, presiona en *"más información"*
    ![](/Images/Descarga4.png)

    Y después de esto, da click en *"Ejecutar de todas formas"*
    ![](/Images/Descarga5.png)


